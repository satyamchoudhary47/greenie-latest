import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { devicePageRoutingModule } from "./device-routing.module";

import { devicePage } from "./device.page";
import { walletPageModule } from "../walletTab/wallet.module";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, devicePageRoutingModule],
  declarations: [devicePage],
})
export class devicePageModule {}
