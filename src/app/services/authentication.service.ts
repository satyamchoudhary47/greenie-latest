import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authState = new BehaviorSubject(false);
  authState2 = new BehaviorSubject(false);

  constructor(
    private storage: Storage,
    private platform: Platform,
    public toastController: ToastController
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  async ifLoggedIn() {
    console.log("check for login data")
    await this.storage.get("currentUser").then(async (res) => {
      if (res) {
        console.log("res",res)
        this.authState.next(true);
      }
    });
  }

  login(){
    this.authState.next(true);

  }

 

  isAuthenticated() {
    return this.authState.value;
  }



}