import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IntialScanPage } from './intial-scan.page';

describe('IntialScanPage', () => {
  let component: IntialScanPage;
  let fixture: ComponentFixture<IntialScanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntialScanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IntialScanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
