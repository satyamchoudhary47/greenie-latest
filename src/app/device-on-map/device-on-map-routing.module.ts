import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceOnMapPage } from './device-on-map.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceOnMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceOnMapPageRoutingModule {}
