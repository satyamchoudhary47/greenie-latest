import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeviceOnMapPageRoutingModule } from './device-on-map-routing.module';

import { DeviceOnMapPage } from './device-on-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeviceOnMapPageRoutingModule
  ],
  declarations: [DeviceOnMapPage]
})
export class DeviceOnMapPageModule {}
