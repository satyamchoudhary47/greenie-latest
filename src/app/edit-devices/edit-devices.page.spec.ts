import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditDevicesPage } from './edit-devices.page';

describe('EditDevicesPage', () => {
  let component: EditDevicesPage;
  let fixture: ComponentFixture<EditDevicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDevicesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditDevicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
