import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddChargerPage } from './add-charger.page';

describe('AddChargerPage', () => {
  let component: AddChargerPage;
  let fixture: ComponentFixture<AddChargerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChargerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddChargerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
