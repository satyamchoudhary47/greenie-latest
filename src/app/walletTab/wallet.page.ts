import { Component, OnInit, ViewChild } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import { CommonService } from "../common.service";
import * as moment from "moment";
import { SuccesAndFailComponent } from "../succes-and-fail/succes-and-fail.component";

declare var RazorpayCheckout: any;
// declare var Razorpay = RazorpayCheckout.initWithKey("razorpayTestKey", andDelegatcveWithData: self);
declare var Razorpay: any;

@Component({
  selector: "app-tab3",
  templateUrl: "./wallet.page.html",
  styleUrls: ["./wallet.page.scss"],
})
export class walletPage implements OnInit {
  TodayDate = Date();
  walletHistory: any = [];
  userBalance: any;

  number = "1000";

  // month = parseInt(moment(new Date()).format("MM"));
  // year = parseInt(moment(new Date()).format("YYYY"));

  maxDate = new Date().toISOString();

  month = parseInt(moment(new Date()).format("MM"));
  year = parseInt(moment(new Date()).format("YYYY"));

  @ViewChild("dateTime") sTime;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public service: CommonService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    console.log("ionViewDidEnter called");
    this.service.getUserBalance(this.month, this.year);
  }

  selectDate(data) {
    console.log("data", data);
    console.log("data", moment(data).format("MM"));
    console.log("data", moment(data).format("YYYY"));

    this.month = parseInt(moment(data).format("MM"));
    this.year = parseInt(moment(data).format("YYYY"));

    this.ionViewDidEnter();
  }

  async openDate() {
    this.sTime.open();
  }

  async addMoney() {
    console.log("add monmey");
    // const alert = await this.alertController.create({
    //   cssClass: "alertsHeading",
    //   header: "Pay Money",
    //   inputs: [
    //     {
    //       name: "name1",
    //       type: "number",
    //       disabled: true,
    //       placeholder: this.service.userBalance,
    //     },
    //   ],
    // buttons: [
    //   {
    //     text: "Cancel",
    //     role: "cancel",
    //     cssClass: "ion-color-secondary",
    //     handler: () => {
    //       console.log("Confirm Cancel");
    //     },
    //   },
    //   {
    //     text: "Pay",
    //     cssClass: "btnWarning",
    //     handler: (alt) => {
    //       console.log("Confirm amount", alt.name1);

    //call webservice to get order id;

    this.service.getOrderIdForRazorPay();
    setTimeout(() => {
      console.log("do Payment we got order id");
      if (this.service.orderId != null)
        this.DoPayment(this.service.userBalance, this.service.orderId);
    }, 500);
    //       },
    //     },
    //   ],
    // });

    // await alert.present();
  }

  DoPayment(amount, orderid) {
    console.log("orderid >>>>", orderid);
    console.log("amount >>>>", amount);
    console.log("email",this.service.emailFromServer) 
    console.log("email",this.service.emailFromServer) 
    var obj = this;
    var options = {
      description: "Make payment",
      image: "http://www.greenie-energy.com/img/logo.png",
      currency: "INR",
      key: "rzp_test_ldrYEpX9KJkb5u",
      // key: 'rzp_live_OZw2DWTURuD3J8',
      amount: amount * 100,
      order_id: orderid,
      name: "Greenie",
      prefill: {
        email: this.service.emailFromServer,
        contact: "+91" + this.service.mobileNumber, 
      },
      theme: {
        color: "#447F08",
      },
    };
    console.log("options",options)

    var successCallback = async function (success) {
      // alert("payment_id: " + success.razorpay_payment_id);
      console.log("success message", success);
      console.log("payment id", success.razorpay_payment_id);
      console.log("payment order id", success.razorpay_order_id);
      console.log("payment signature id", success.razorpay_signature);
      await obj.service.ClearDues(
        success.razorpay_payment_id,
        success.razorpay_order_id,
        // orderid,
        success.razorpay_signature,
        success
      );

      setTimeout(() => {
        obj.ionViewDidEnter();
      }, 500);
      // var orderId = success.razorpay_order_id
      // var signature = success.razorpay_signature
    };
    var cancelCallback = function (error) {
      console.log("error", error);
      alert(error.error.description);
    };
    RazorpayCheckout.on("payment.success", successCallback);
    RazorpayCheckout.on("payment.cancel", cancelCallback);
    RazorpayCheckout.open(options);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // var razorpayObject = new Razorpay(options);
    // console.log(razorpayObject);
    // razorpayObject.on("payment.failed", function (response) {
    //   console.log(response);
    //   alert("This step of Payment Failed");
    // });

    // razorpayObject.open();

    // document.getElementById("pay-button").onclick = function (e) {
    // e.preventDefault();
    // };
  }

  async openModal(data) {
    console.log("modal data", data);
    const modal = await this.modalController.create({
      component: SuccesAndFailComponent,
      backdropDismiss: true,
      swipeToClose: true,
      //  cssClass: "my-custom-modal-cssforDevice",
      cssClass: "my-custom-modal-cssforTransaction",
      componentProps: { data: data },
      // componentProps: { data: 'success' },
    });
    await modal.present();
  }
}
