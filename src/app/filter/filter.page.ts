import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-filter",
  templateUrl: "./filter.page.html",
  styleUrls: ["./filter.page.scss"],
})
export class FilterPage implements OnInit {
  startDate: any;
  endDate: any;

  constructor(public modal: ModalController) {}

  ngOnInit() {}

  close() {
    this.modal.dismiss({});
  }

  filter() {
    var data = {
      startDate: this.startDate,
      endDate: this.endDate,
    };

    this.modal.dismiss(data);
  }

}
